import cv2
import numpy as np


class ImageFeature:
    point = np.array([[0.0, 0.0]])
    descriptor = []
    distance = 1000.0

    def __init__(self, point, desc, dist):
        self.point = np.copy(point)
        self.descriptor = np.copy(desc)
        self.distance = dist


def obtaindescriptors(image_float32, window_size, feature_coordinates):

    feature_list = []
    w = window_size/2

    x_dim = image_float32.shape[0]
    y_dim = image_float32.shape[1]

    for k in feature_coordinates:
        col = int(k[0, 0])
        row = int(k[0, 1])

        if col < window_size/2 or row < window_size/2 or col > x_dim - window_size/2 or row > y_dim - window_size/2:
            continue

        point = np.array([[float(col), float(row)]])

        descriptor = image_float32[row-w:row+w, col-w:col+w]
        descriptor = np.copy(descriptor)
        descriptor = descriptor.flatten()

        img_feat = ImageFeature(point, descriptor, 0.0)
        feature_list.append(img_feat)

    return feature_list


def printfeatures(color_image, window_size, feature_list):

    new_image = np.copy(color_image)

    for num in range(feature_list.shape[0]):

        col = int(feature_list[num, 0, 0])
        row = int(feature_list[num, 0, 1])
        w = window_size/2

        submatrix = new_image[row-w:row+w, col-w:col+w]

        for x in range(window_size):
            for y in range(window_size):
                if x == 0 or x == window_size - 1 or y == 0 or y == window_size - 1:
                    submatrix[x, y] = [0, 0, 255]

    return new_image


def creatematchinglist(feature_list1, feature_list2):
    match_list = []

    for f1 in feature_list1:
        for f2 in feature_list2:

            distance = np.linalg.norm(np.subtract(f1.descriptor, f2.descriptor))

            feat1 = ImageFeature(f1.point, f1.descriptor, distance)
            feat2 = ImageFeature(f2.point, f2.descriptor, distance)

            match_list.append([feat1, feat2])

    return match_list


def selectbestmatches(feature_list1, feature_list2, max_matches):
    left_points = []
    right_points = []

    match_list = creatematchinglist(feature_list1, feature_list2)
    match_list = sorted(match_list, key=lambda feature: feature[0].distance)

    for i in range(max_matches):
        if i == 0:
            left_points = np.copy(match_list[0][0].point)
            right_points = np.copy(match_list[0][1].point)
        else:
            left_points = np.append(left_points, match_list[i][0].point, axis=0)
            right_points = np.append(right_points, match_list[i][1].point, axis=0)

    left_points = np.reshape(left_points, (-1, 1, 2))
    right_points = np.reshape(right_points, (-1, 1, 2))

    return left_points, right_points


def getinliers(left_points, right_points, mask):
    array_length = left_points.shape[0]

    inlier_number = 0
    left_inliers = []
    right_inliers = []

    for i in range(array_length):
        if mask[i] == 1:
            if inlier_number == 0:
                left_inliers = np.copy(left_points[i])
                right_inliers = np.copy(right_points[i])
            else:
                left_inliers = np.append(left_inliers, left_points[i], axis=0)
                right_inliers = np.append(right_inliers, right_points[i], axis=0)

            inlier_number = inlier_number + 1

    left_inliers = np.reshape(left_inliers, (-1, 1, 2))
    right_inliers = np.reshape(right_inliers, (-1, 1, 2))

    return left_inliers, right_inliers, inlier_number


def drawmatches(img_left, img_right, left_points, right_points, window_size):
    resulting_image = np.copy(img_left)
    resulting_image = np.append(resulting_image, img_right, axis=1)

    point_offset = float(img_left.shape[1])
    points_right = np.copy(right_points)

    for i in range(right_points.shape[0]):
        points_right[i, 0] = np.add(right_points[i, 0], [point_offset, 0])

        left_x = int(left_points[i, 0, 0])
        left_y = int(left_points[i, 0, 1])
        right_x = int(points_right[i, 0, 0])
        right_y = int(points_right[i, 0, 1])

        B = np.random.randint(0, high=255)
        G = np.random.randint(0, high=255)
        R = np.random.randint(0, high=255)

        line_color = (B, G, R)

        cv2.line(resulting_image, (left_x, left_y),
                 (right_x, right_y), color=line_color,
                 thickness=1)

    point_array = np.append(left_points, points_right, axis=0)
    resulting_image = printfeatures(resulting_image, window_size, point_array)

    return resulting_image


def stitchimages(img_left, img_right):

    left_gray = cv2.cvtColor(img_left, cv2.COLOR_BGR2GRAY)
    right_gray = cv2.cvtColor(img_right, cv2.COLOR_BGR2GRAY)

    ret, mask1 = cv2.threshold(left_gray, 1, 1, cv2.THRESH_BINARY)
    ret, mask2 = cv2.threshold(right_gray, 1, 1, cv2.THRESH_BINARY)

    overlap_mask = np.multiply(mask1, mask2)
    ret, mask_inverted = cv2.threshold(overlap_mask, 0, 1, cv2.THRESH_BINARY_INV)

    overlap_mask = cv2.merge((overlap_mask, overlap_mask, overlap_mask))
    mask_inverted = cv2.merge((mask_inverted, mask_inverted, mask_inverted))

    img_sum = np.add(img_left, img_right)
    img_sum = np.multiply(img_sum, mask_inverted)

    img_average = cv2.addWeighted(img_left, 0.5, img_right, 0.5, 0.0)
    img_average = np.multiply(img_average, overlap_mask)

    stitched_image = np.add(img_sum, img_average)

    return stitched_image


def computeresidual(src_points, dst_points, Homography_Matrix):
    residual = 0.0

    src_points = np.append(src_points, np.ones((src_points.shape[0], src_points.shape[1], 1), dtype=np.float), axis=2)
    dst_points = np.append(dst_points, np.ones((dst_points.shape[0], dst_points.shape[1], 1), dtype=np.float), axis=2)

    num = src_points.shape[0]

    for i in range(num):
        homogeneous_src = src_points[i].reshape(3, 1)
        homogeneous_dst = dst_points[i].reshape(3, 1)

        res = np.matmul(Homography_Matrix, homogeneous_src)
        res = res/res[2]
        diff = np.subtract(homogeneous_dst, res)
        diff_t = diff.reshape(1, 3)

        print homogeneous_dst, res

        total_squared_distance = np.matmul(diff_t, diff)
        residual = residual + total_squared_distance

    return residual


def find_putative_matches(image1, image2, harris_neighbor_size, threshold, alpha,
                                    num_features, features_windowsize, max_matches):

    im1_gray = np.float32(cv2.cvtColor(image1, cv2.COLOR_BGR2GRAY))
    im2_gray = np.float32(cv2.cvtColor(image2, cv2.COLOR_BGR2GRAY))

    # Obtain best features from Grayscale image

    im1_harris_features = cv2.goodFeaturesToTrack(image=im1_gray,
                                                  maxCorners=num_features,
                                                  qualityLevel=threshold,
                                                  minDistance=harris_neighbor_size,
                                                  useHarrisDetector=True,
                                                  k=alpha)

    im2_harris_features = cv2.goodFeaturesToTrack(image=im2_gray,
                                                  maxCorners=num_features,
                                                  qualityLevel=threshold,
                                                  minDistance=harris_neighbor_size,
                                                  useHarrisDetector=True,
                                                  k=alpha)

    # Create Descriptors

    im1_descriptors = obtaindescriptors(image_float32=im1_gray,
                                        window_size=features_windowsize,
                                        feature_coordinates=im1_harris_features)

    im2_descriptors = obtaindescriptors(image_float32=im2_gray,
                                        window_size=features_windowsize,
                                        feature_coordinates=im2_harris_features)

    points1, points2 = selectbestmatches(im1_descriptors, im2_descriptors, max_matches)

    return points1, points2
