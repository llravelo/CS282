# Using python 2.7.13 and OpenCV 3.2.0
import cv2
import numpy as np
from matplotlib import pyplot as plt
from mpl_toolkits.mplot3d import Axes3D

import pa3_functions as calibfunc
import pa2_functions as stfunc

# Harris Detector Parameters

NEIGHBOR_SIZE = 8
HARRIS_THRESHOLD = 0.1
ALPHA = 0.06
NUM_FEATURES = 200

# Feature Descriptor

WINDOW_SIZE = 4
MAX_MATCHES = 80

im1 = cv2.imread('./epipairs/library1.jpg', cv2.IMREAD_COLOR)
im2 = cv2.imread('./epipairs/library2.jpg', cv2.IMREAD_COLOR)

library_filename = './epipairs/library_matches.txt'
cam1_filename = './epipairs/library1_camera.txt'
cam2_filename = './epipairs/library2_camera.txt'

# Matches from the Library
library_points1, library_points2 = calibfunc.obtain_points(library_filename)

# Obtain Matches from PA2
harris_points1, harris_points2 = stfunc.find_putative_matches(image1=im1, image2=im2, harris_neighbor_size=NEIGHBOR_SIZE,
                                                            threshold=HARRIS_THRESHOLD, alpha=ALPHA,
                                                            num_features=NUM_FEATURES, features_windowsize=WINDOW_SIZE,
                                                            max_matches=MAX_MATCHES)

# Find the Fundamental matrices

# Library Fundamental Matrix, least median squares
F_library, library_mask = cv2.findFundamentalMat(library_points1, library_points2, cv2.FM_LMEDS)

# Inlier matches obtained using matches
Homography_matrix, homo_mask = cv2.findHomography(harris_points1, harris_points2, cv2.RANSAC, 5.0)
harris_inliers1, harris_inliers2, inlier_number = stfunc.getinliers(harris_points1, harris_points2, homo_mask)

# Fundamental Matrix obtained, using the 8-point algorithm
F_harris, fundamental_mask = cv2.findFundamentalMat(harris_inliers1, harris_inliers2, cv2.FM_8POINT)
matching_points1, matching_points2, num_points = stfunc.getinliers(harris_inliers1, harris_inliers2, fundamental_mask)

# Draw Matches
library_matches = stfunc.drawmatches(im1, im2, library_points1, library_points2, WINDOW_SIZE)
harris_matches = stfunc.drawmatches(im1, im2, matching_points1, matching_points2, WINDOW_SIZE)

print "Fundamental Matrix obtained from library matches:"
print F_library, '\n'

# Compute for the residual
library_residual = calibfunc.compute_epipole_residual(F_library, library_points1, library_points2)
print "Library point Residual: %f" % library_residual
print ''

print "Fundamental Matrix obtained from Harris putative matches:"
print F_harris, '\n'

matching_residual = calibfunc.compute_epipole_residual(F_harris, matching_points1, matching_points2)
print "Inlier point Residual: %f" % matching_residual
print ''

# Obtain 3D world coordinates of camera centers
cam1_mat, cam2_mat = calibfunc.load_camera_matrices(cam1_filename, cam2_filename)
center1 = calibfunc.find_camera_center(cam1_mat)
center2 = calibfunc.find_camera_center(cam2_mat)

print "Camera 1 is located at (X, Y, Z): %f, %f, %f" % (center1[0, 0, 0], center1[0, 0, 1], center1[0, 0, 2])
print "Camera 2 is located at (X, Y, Z): %f, %f, %f\n" % (center2[0, 0, 0], center2[0, 0, 1], center2[0, 0, 2])

print "Triangulating 3D points..."

# Obtain normalized 3D world points from library matches
points3dt = cv2.triangulatePoints(cam1_mat, cam2_mat, library_points1, library_points2)
points3d = np.transpose(points3dt)
points3d = np.reshape(points3d, (-1, 1, 4))
points3d_normalized = calibfunc.normalize(points3d)

# Normalized 3D world points for the inlier matches
inlier3dt = cv2.triangulatePoints(cam1_mat, cam2_mat, matching_points1, matching_points2)
inlier3d = np.transpose(inlier3dt)
inlier3d = np.reshape(inlier3d, (-1, 1, 4))
inlier3d_normalized = calibfunc.normalize(inlier3d)

# Compute for the residual of the reprojected points
# Residual is the total squared distances
# between reprojected points and library points

reprojected_points1 = calibfunc.reproject_3dpoints(points3d_normalized, cam1_mat)
reprojected_points2 = calibfunc.reproject_3dpoints(points3d_normalized, cam2_mat)

reprojected_inlier1 = calibfunc.reproject_3dpoints(inlier3d_normalized, cam1_mat)
reprojected_inlier2 = calibfunc.reproject_3dpoints(inlier3d_normalized, cam2_mat)

distance1 = calibfunc.compute_mean_squared_distance(library_points1, reprojected_points1)
distance2 = calibfunc.compute_mean_squared_distance(library_points2, reprojected_points2)

inlier_dist1 = calibfunc.compute_mean_squared_distance(matching_points1, reprojected_inlier1)
inlier_dist2 = calibfunc.compute_mean_squared_distance(matching_points2, reprojected_inlier2)

library_residual = distance1 + distance2
inlier_residual = inlier_dist1 + inlier_dist2

print "Reprojection residual for the library matches: %f" % library_residual
print "Reprojection residual for the inlier matches: %f" % inlier_residual

# Print 3D points
calibfunc.print_csv(center1, center2, points3d_normalized, 'library_points.csv')
calibfunc.print_csv(center1, center2, inlier3d_normalized, 'inlier_points.csv')

cv2.imwrite('./matches/library.jpg', library_matches)
cv2.imwrite('./matches/harris.jpg', harris_matches)