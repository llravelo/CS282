import cv2
import numpy as np
import math


def obtain_points(filename):
    f = open(filename, 'r')

    im1_points = []
    im2_points = []

    for line in f:
        elements = line.split()

        im1_x = float(elements[0])
        im1_y = float(elements[1])
        im2_x = float(elements[2])
        im2_y = float(elements[3])

        im1_points = np.append(im1_points, [im1_x, im1_y], axis=0)
        im2_points = np.append(im2_points, [im2_x, im2_y], axis=0)

    im1_points = np.reshape(im1_points, (-1, 1, 2))
    im2_points = np.reshape(im2_points, (-1, 1, 2))

    f.close()

    return im1_points, im2_points


def draw_points(src, points):

    dest = np.copy(src)
    num_points = points.shape[0]

    for i in range(num_points):
        point_x = int(points[i, 0, 0])
        point_y = int(points[i, 0, 1])

        cv2.circle(dest, (point_x, point_y), 0, (255, 255, 255), -1)

    return dest


def load_camera_matrices(camera1_file, camera2_file):
    f1 = open(camera1_file, 'r')
    f2 = open(camera2_file, 'r')

    camera1_matrix = []
    camera2_matrix = []

    for line in f1:
        elements = line.split()
        for iter in elements:
            num = float(iter)
            camera1_matrix = np.append(camera1_matrix, [num], axis=0)

    for line in f2:
        elements = line.split()
        for iter in elements:
            num = float(iter)
            camera2_matrix = np.append(camera2_matrix, [num], axis=0)

    camera1_matrix = np.reshape(camera1_matrix, (3, 4))
    camera2_matrix = np.reshape(camera2_matrix, (3, 4))

    return camera1_matrix, camera2_matrix


def find_camera_center(cam_matrix):
    # Camera center C can be derived from M
    # Through the Equation MC = 0
    # We can set the last value of the
    # Homogeneous coordinate C to be 1.0

    Q_matrix = cam_matrix[:, 0:3]

    # b_vector is the last column of M, which is multiplied by 1.0 (from C)
    # Move to the other side

    b_vector = (-1.0)*cam_matrix[:, 3]
    b_vector = np.reshape(b_vector, (3, 1))

    # We are left with the Q submatrix
    # Solve for Q*[x y z]T = bT

    center = np.linalg.solve(Q_matrix, b_vector)
    center = np.append(center, [1.0])
    center = np.reshape(center, (1, 1, 4))

    return center


def normalize(homogeneous_array):
    array_size = homogeneous_array.shape[0]
    dim = homogeneous_array.shape[2]

    normalized_array = []
    for i in range(array_size):
        normalized_array = np.append(normalized_array, homogeneous_array[i, 0]/homogeneous_array[i, 0, dim-1])

    normalized_array = np.reshape(normalized_array, (-1, 1, dim))

    return normalized_array


def trim(homogeneous_array):
    dim = homogeneous_array.shape[2]

    trimmed_array = homogeneous_array[:, :, 0:dim-1]
    output = np.copy(trimmed_array)

    return output


def homogenize(real_coordinate_array):
    dim = real_coordinate_array.shape[2]
    array_length = real_coordinate_array.shape[0]

    homogenized_array = np.reshape(real_coordinate_array, (-1, dim))
    homogeneous_coordinate = np.ones((array_length, 1), dtype=np.float)

    homogenized_array = np.append(homogenized_array, homogeneous_coordinate, axis=1)
    homogenized_array = np.reshape(homogenized_array, (-1, 1, dim+1))

    return homogenized_array


def compute_epipole_residual(fundamental_matrix, src_pts, dest_pts):
    # Function computes for the epipole residual
    # Which is the mean distance of the points

    total_residual = 0.0
    length = src_pts.shape[0]

    src_homog = homogenize(src_pts)

    for i in range(length):
        source = np.copy(src_homog[i, 0])
        source = np.reshape(source, (3, 1))
        dest = dest_pts[i, 0]

        epipolar_line = np.matmul(fundamental_matrix, source)
        epipolar_line = np.reshape(epipolar_line, (3, 1))

        A = epipolar_line[0]
        B = epipolar_line[1]
        C = epipolar_line[2]

        # Find x and y projection of the destination point
        # in the epipolar line
        x1 = dest[0]
        y1 = (A*x1 + C)/((-1.0)*B)
        y2 = dest[1]
        x2 = (B*y2 + C)/((-1.0)*A)

        pt1 = np.array([x1, y1], dtype=np.float)
        pt2 = np.array([x2, y2], dtype=np.float)

        # Obtain unit vector describing the line

        vect = np.subtract(pt1, pt2)
        dist = math.sqrt(np.sum(np.square(vect)))
        unit_vector = vect/dist

        # Obtain vector describing the point
        dest_vector = np.subtract(dest, pt1)

        # Distance is magnitude of cross product
        # Between unit_vector and dest_vector
        squared_distance = np.sum(np.square(np.cross(unit_vector, dest_vector)))
        total_residual = total_residual + squared_distance

    # Get the average squared distances
    average_residual = total_residual/float(length)
    return average_residual


def reproject_3dpoints(points3d_array, camera_matrix):
    # Reprojects 3D world point to its corresponding
    # pixel point in the image plane

    points2d_array = []
    length = points3d_array.shape[0]

    # Projection is a simple matrix multiplication
    # Do for all 3D points
    for i in range(length):
        point3d = points3d_array[i, 0]
        point3d = np.reshape(point3d, (4, 1))
        point2d = np.matmul(camera_matrix, point3d)

        points2d_array = np.append(points2d_array, point2d)

    points2d_array = np.reshape(points2d_array, (-1, 1, 3))
    points2d_array = normalize(points2d_array)
    points2d_array = trim(points2d_array)

    return points2d_array


def compute_mean_squared_distance(point1_array, point2_array):
    # Computes the mean squared distance between
    # two matching point sets

    total_squared_distance = 0.0
    length = point1_array.shape[0]

    for i in range(length):
        total_squared_distance = total_squared_distance + \
                                 np.sum(np.square(np.subtract(point1_array[i, 0],
                                                              point2_array[i, 0])))

    mean_squared_distance = total_squared_distance/float(length)
    return mean_squared_distance


def print_csv(cam1, cam2, coordinates3d, filename):
    # Outputs 3D points, including camera centers,
    # To a .csv file

    file = open(filename, 'w')

    string = "x,y,z\n"
    file.write(string)
    string = "%f,%f,%f\n" % (cam1[0, 0, 0], cam1[0, 0, 1], cam1[0, 0, 2])
    file.write(string)
    string = "%f,%f,%f\n" % (cam2[0, 0, 0], cam2[0, 0, 1], cam2[0, 0, 2])
    file.write(string)

    len = coordinates3d.shape[0]
    for i in range(len):
        string = "%f,%f,%f\n" % (coordinates3d[i, 0, 0], coordinates3d[i, 0, 1], coordinates3d[i, 0, 2])
        file.write(string)
    file.close()