import cv2
import numpy as np


class ImageFeature:
    point = np.array([[0.0, 0.0]])
    descriptor = []
    distance = 1000.0

    def __init__(self, point, desc, dist):
        self.point = np.copy(point)
        self.descriptor = np.copy(desc)
        self.distance = dist


def obtaindescriptors(image_float32, window_size, feature_coordinates):
    # Obtain pixel values a 'window_size' value around the feature coordinate

    feature_list = []
    w = window_size/2

    for k in feature_coordinates:
        col = int(k[0, 0])
        row = int(k[0, 1])

        point = np.array([[float(col), float(row)]])

        descriptor = image_float32[row-w:row+w, col-w:col+w]
        descriptor = np.copy(descriptor)
        descriptor = descriptor.flatten()

        img_feat = ImageFeature(point, descriptor, 0.0)
        feature_list.append(img_feat)

    return feature_list


def printfeatures(color_image, window_size, feature_list):
    # Prints image, containing the selected features

    new_image = np.copy(color_image)

    for num in range(feature_list.shape[0]):

        col = int(feature_list[num, 0, 0])
        row = int(feature_list[num, 0, 1])
        w = window_size/2

        submatrix = new_image[row-w:row+w, col-w:col+w]

        for x in range(window_size):
            for y in range(window_size):
                if x == 0 or x == window_size - 1 or y == 0 or y == window_size - 1:
                    submatrix[x, y] = [0, 0, 255]

    return new_image


def creatematchinglist(feature_list1, feature_list2):
    # Create a matching list, containing every possible matches
    # between features

    match_list = []

    for f1 in feature_list1:
        for f2 in feature_list2:

            distance = np.linalg.norm(np.subtract(f1.descriptor, f2.descriptor))

            feat1 = ImageFeature(f1.point, f1.descriptor, distance)
            feat2 = ImageFeature(f2.point, f2.descriptor, distance)

            match_list.append([feat1, feat2])

    return match_list


def selectbestmatches(feature_list1, feature_list2, max_matches):
    # Sort matches according to increasing euclidean distance
    # between the pixelss
    # Select the best matches

    left_points = []
    right_points = []

    match_list = creatematchinglist(feature_list1, feature_list2)
    match_list = sorted(match_list, key=lambda feature: feature[0].distance)

    for i in range(max_matches):
        if i == 0:
            left_points = np.copy(match_list[0][0].point)
            right_points = np.copy(match_list[0][1].point)
        else:
            left_points = np.append(left_points, match_list[i][0].point, axis=0)
            right_points = np.append(right_points, match_list[i][1].point, axis=0)

    left_points = np.reshape(left_points, (-1, 1, 2))
    right_points = np.reshape(right_points, (-1, 1, 2))

    return left_points, right_points


def getinliers(left_points, right_points, mask):
    # Obtain inlier list from the mask

    array_length = left_points.shape[0]

    inlier_number = 0
    left_inliers = []
    right_inliers = []

    for i in range(array_length):
        if mask[i] == 1:
            if inlier_number == 0:
                left_inliers = np.copy(left_points[i])
                right_inliers = np.copy(right_points[i])
            else:
                left_inliers = np.append(left_inliers, left_points[i], axis=0)
                right_inliers = np.append(right_inliers, right_points[i], axis=0)

            inlier_number = inlier_number + 1

    left_inliers = np.reshape(left_inliers, (-1, 1, 2))
    right_inliers = np.reshape(right_inliers, (-1, 1, 2))

    return left_inliers, right_inliers, inlier_number


def drawmatches(img_left, img_right, left_points, right_points, window_size):
    # Display image, containing the respective matching features

    resulting_image = np.copy(img_left)
    resulting_image = np.append(resulting_image, img_right, axis=1)

    point_offset = float(img_left.shape[1])

    for i in range(right_points.shape[0]):
        right_points[i, 0] = np.add(right_points[i, 0], [point_offset, 0])

        left_x = int(left_points[i, 0, 0])
        left_y = int(left_points[i, 0, 1])
        right_x = int(right_points[i, 0, 0])
        right_y = int(right_points[i, 0, 1])

        B = np.random.randint(0, high=255)
        G = np.random.randint(0, high=255)
        R = np.random.randint(0, high=255)

        line_color = (B, G, R)

        cv2.line(resulting_image, (left_x, left_y),
                 (right_x, right_y), color=line_color,
                 thickness=2)

    point_array = np.append(left_points, right_points, axis=0)
    resulting_image = printfeatures(resulting_image, window_size, point_array)

    return resulting_image


def stitchimages(img_left, img_right):
    # Stitches both images

    left_gray = cv2.cvtColor(img_left, cv2.COLOR_BGR2GRAY)
    right_gray = cv2.cvtColor(img_right, cv2.COLOR_BGR2GRAY)

    # Create Overlap and inverted Overlap mask
    ret, mask1 = cv2.threshold(left_gray, 1, 1, cv2.THRESH_BINARY)
    ret, mask2 = cv2.threshold(right_gray, 1, 1, cv2.THRESH_BINARY)

    overlap_mask = np.multiply(mask1, mask2)
    ret, mask_inverted = cv2.threshold(overlap_mask, 0, 1, cv2.THRESH_BINARY_INV)

    # For each channel of the 'BGR' image
    overlap_mask = cv2.merge((overlap_mask, overlap_mask, overlap_mask))
    mask_inverted = cv2.merge((mask_inverted, mask_inverted, mask_inverted))

    # Simply add the non overlapping parts
    img_sum = np.add(img_left, img_right)
    img_sum = np.multiply(img_sum, mask_inverted)

    # Take the average between pixel values
    # on overlapping images
    img_average = cv2.addWeighted(img_left, 0.5, img_right, 0.5, 0.0)
    img_average = np.multiply(img_average, overlap_mask)

    stitched_image = np.add(img_sum, img_average)

    return stitched_image


def computeresidual(src_points, dst_points, matrix):
    total_residual = 0.0

    src_points = np.append(src_points, np.ones((src_points.shape[0], src_points.shape[1], 1), dtype=np.float), axis=2)
    dst_points = np.append(dst_points, np.ones((dst_points.shape[0], dst_points.shape[1], 1), dtype=np.float), axis=2)

    num = src_points.shape[0]

    for i in range(num):
        homogeneous_src = src_points[i].reshape(3, 1)
        homogeneous_dst = dst_points[i].reshape(3, 1)

        res = np.matmul(matrix, homogeneous_src)
        res = res/res[2]
        diff = np.subtract(homogeneous_dst, res)
        diff_t = diff.reshape(1, 3)

        total_squared_distance = np.matmul(diff_t, diff)
        total_residual = total_residual + total_squared_distance

    average_residual = total_residual/float(num)

    return average_residual
