# Using Python 2.7.13 and OpenCV 3.2.1
import cv2
import numpy as np
import functions as stfunc

# Harris Detector Parameters

NEIGHBOR_SIZE = 8
HARRIS_THRESHOLD = 0.1
ALPHA = 0.06
NUM_FEATURES = 250

# Feature Descriptor

WINDOW_SIZE = 10
MAX_MATCHES = 100

# Time start
clock_frequency = cv2.getTickFrequency()
start_time = cv2.getTickCount()

iml = cv2.imread('tower_left.jpg', cv2.IMREAD_COLOR)
imr = cv2.imread('tower_right.jpg', cv2.IMREAD_COLOR)

iml_gray = cv2.cvtColor(iml, cv2.COLOR_BGR2GRAY)
imr_gray = cv2.cvtColor(imr, cv2.COLOR_BGR2GRAY)

iml_gray = np.float32(iml_gray)
imr_gray = np.float32(imr_gray)

# Obtain best features from Grayscale image

iml_harris = cv2.goodFeaturesToTrack(image=iml_gray,
                                     maxCorners=NUM_FEATURES,
                                     qualityLevel=HARRIS_THRESHOLD,
                                     minDistance=NEIGHBOR_SIZE,
                                     useHarrisDetector=True,
                                     k=ALPHA)

imr_harris = cv2.goodFeaturesToTrack(image=imr_gray,
                                     maxCorners=NUM_FEATURES,
                                     qualityLevel=HARRIS_THRESHOLD,
                                     minDistance=NEIGHBOR_SIZE,
                                     useHarrisDetector=True,
                                     k=ALPHA)

# Create Descriptors

feat_l = stfunc.obtaindescriptors(image_float32=iml_gray,
                                  window_size=WINDOW_SIZE,
                                  feature_coordinates=iml_harris)

feat_r = stfunc.obtaindescriptors(image_float32=imr_gray,
                                  window_size=WINDOW_SIZE,
                                  feature_coordinates=imr_harris)

# Create an image showing the features

iml_feat = stfunc.printfeatures(color_image=iml,
                                window_size=WINDOW_SIZE,
                                feature_list=iml_harris)

imr_feat = stfunc.printfeatures(color_image=imr,
                                window_size=WINDOW_SIZE,
                                feature_list=imr_harris)

# Select best matches, defined by MAX_MATCHES
left_points, right_points = stfunc.selectbestmatches(
    feat_l, feat_r, MAX_MATCHES)

# Estimate Homography Matrix, from the putative matches
Homography_matrix, mask = cv2.findHomography(
    right_points, left_points, cv2.RANSAC, 5.0)

left_inliers_homography, \
    right_inliers_homography, \
    inlier_number_homography = stfunc.getinliers(
        left_points, right_points, mask)

residual_homography = stfunc.computeresidual(src_points=right_inliers_homography, dst_points=left_inliers_homography,
                                             matrix=Homography_matrix)

print "Estimated Homography Matrix:"
print Homography_matrix, '\n'
print "There are %d inliers" % inlier_number_homography
print "The Homography residual is %f\n" % residual_homography

# Get Affine Matrix from the inlier matches

left_inliers_affine = np.copy(left_inliers_homography[:3]).astype(np.float32)
right_inliers_affine = np.copy(right_inliers_homography[:3]).astype(np.float32)

Affine_matrix = cv2.getAffineTransform(
    right_inliers_affine, left_inliers_affine)

bottom_row = np.array([[0.0, 0.0, 1.0]], dtype=np.float)
Affine_matrix = np.append(Affine_matrix, bottom_row, axis=0)

residual_affine = stfunc.computeresidual(src_points=right_inliers_homography, dst_points=left_inliers_homography,
                                         matrix=Affine_matrix)

print "Affine Matrix:"
print Affine_matrix, '\n'
print "There are %d inliers" % inlier_number_homography
print "Affine Residual is %f\n" % residual_affine

# Create an Image containing the matching pairs
# For the putative matches and obtained inliers

img_matches = stfunc.drawmatches(
    iml, imr, left_points, right_points, WINDOW_SIZE)
img_ransac = stfunc.drawmatches(
    iml, imr, left_inliers_homography, right_inliers_homography, WINDOW_SIZE)

# Create the perspective-warped right image, then stitch left and right images

right_warped = cv2.warpPerspective(
    imr, Homography_matrix, (iml.shape[1] * 2, iml.shape[0]))

zero_image = np.zeros(iml.shape, dtype=iml.dtype)
left_image = np.copy(iml)
left_image = np.append(left_image, zero_image, axis=1)

stitched_image = stfunc.stitchimages(left_image, right_warped)

cv2.imwrite('./features/left_features.jpg', iml_feat)
cv2.imwrite('./features/right_features.jpg', imr_feat)

cv2.imwrite('./matches/initial_matches.jpg', img_matches)
cv2.imwrite('./matches/ransac_matches.jpg', img_ransac)

cv2.imwrite('stitched_output.jpg', stitched_image)

# Stop
stop_time = cv2.getTickCount()

total_time = ((stop_time - start_time) / clock_frequency) * 1000
print "Total time taken: %.3f ms" % total_time
